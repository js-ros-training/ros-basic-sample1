#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from PIL import Image as ImagePil
from io import BytesIO
import requests
import numpy
from cv_bridge import CvBridge
import json

class img_generator:
    URL_IMG_RANDOM = "https://source.unsplash.com/random/800*400"

    def __init__(self):
        rospy.init_node('img_generator_node', anonymous=False)
        self.pub = rospy.Publisher('/img_generator/img', Image, queue_size=1)
        self.sub = rospy.Subscriber("/img_generator/input", String, self.process_input)
        self._bridge = CvBridge()
        rospy.loginfo("[img_generator] Started......")
        rospy.spin()

    def process_input(self, data):
        rospy.loginfo("[img_generator] " + str(data))
        self.generate_img()

    def generate_img(self):
        response=requests.get(self.URL_IMG_RANDOM)
        pil_image = ImagePil.open(BytesIO(response.content)).convert('RGB') 
        open_cv_image = numpy.array(pil_image)
        img_msg=self._bridge.cv2_to_imgmsg(open_cv_image, "bgr8")
        self.pub.publish(img_msg)
        
        

if __name__ == '__main__':
    try:
        img_generator()
    except rospy.ROSInterruptException:
        pass