#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String

class talker:

    def __init__(self):
        rospy.init_node('talker_node', anonymous=False)
        self.pub = rospy.Publisher('my_behavior', String, queue_size=1)
        self.sub = rospy.Subscriber("input", String, self.process_input)
        rospy.loginfo("[talker] Started......")
        rospy.spin()

    def process_input(self, data):
        rospy.loginfo("[INPUT] " + str(data))
        if data.data == "PING" or data.data == "CHUCK":
            self.pub.publish(data.data)
        else:
            rospy.loginfo("[INPUT] Only PING and CHUCK cmd available")
            
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass