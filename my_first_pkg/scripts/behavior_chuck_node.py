#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
import requests
import json

class behavior_chuck:
    #URL_CHUCK_JOKE = "http://api.icndb.com/jokes/random"
    URL_CHUCK_JOKE = "https://api.chucknorris.io/jokes/random"

    def __init__(self):
        rospy.init_node('behavior_chuck_node', anonymous=False)
        self.pub_to_img_gene = rospy.Publisher('/img_generator/input', String, queue_size=1)
        self.pub = rospy.Publisher('input', String, queue_size=1)
        self.sub = rospy.Subscriber("my_behavior", String, self.process_behavior)
        rospy.loginfo("[my_behavior] Started......")
        rospy.spin()

    def process_behavior(self, data):
        rospy.loginfo("[my_behavior] " + str(data))
        if data.data == "PING" :
            self.pub.publish("PONG")
        elif data.data == "CHUCK":
            msg = self.get_chuck()
            self.pub_to_img_gene.publish(str(msg))

    def get_chuck(self):
        data = requests.get(self.URL_CHUCK_JOKE)
        tt = json.loads(data.text)
        rospy.loginfo(tt["value"])
        return str(tt["value"])
        

if __name__ == '__main__':
    try:
        behavior_chuck()
    except rospy.ROSInterruptException:
        pass